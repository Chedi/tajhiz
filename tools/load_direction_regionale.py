from saisie.models            import DirectionRegionale
from tools.import_tool        import slugify
from django.contrib.gis.utils import LayerMapping

states_mapping = {
	'label'     : 'NOM_GOUV',
	'geom'      : 'MULTIPOLYGON',
}

tunisia_states = "/var/www/django/tajhiz/fixtures/shapes/drs/drs.shp"

def run(verbose=True):
	lm = LayerMapping(DirectionRegionale, tunisia_states, states_mapping, transform=True, encoding='utf8')
	lm.save(strict=True, verbose=verbose)
	for i in DirectionRegionale.objects.all():
		i.label = slugify(i.label)
		i.save()
