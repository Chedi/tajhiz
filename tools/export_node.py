import xlwt
import MySQLdb

from django.contrib.gis.geos import *
from django.contrib.gis.measure import D

from saisie.models import *

def export_node():
	nodes      = Noeud.objects.all()
	tajhiz_db  = MySQLdb.connect(host="localhost", user="root", passwd="crocmolodoude", db="tajhiz")
	tajhiz_cur = tajhiz_db.cursor()

	book  = xlwt.Workbook(encoding="utf-8")
	sheet = book.add_sheet("panels_by_nodes")

	sheet.write(0,0, "Id signal routier")
	sheet.write(0,1, "Direction")
	sheet.write(0,2, "Noeud")
	sheet.write(0,3, "Type")
	sheet.write(0,4, "Noeud longitude")
	sheet.write(0,5, "Noeud latitude")

	current_row = 1

	for node in nodes:
		radius_panels = Event.objects.filter(position__distance_lte=(node.position, 100))
		for panel in radius_panels:
			tajhiz_cur.execute("select s.ID, d.direction from SIGNALROUTIER s inner join data d on d.id = s.DATA3B_id where abs(d.gpx_lon-%s) < 0.0001 and abs(d.gpx_lat-%s) < 0.0001",(panel.position.x, panel.position.y))
			result = tajhiz_cur.fetchone()
			if result:
				sheet.write(current_row,0, result[0])
				sheet.write(current_row,1, result[1])
				sheet.write(current_row,2, node.label)
				sheet.write(current_row,3, node.type)
				sheet.write(current_row,4, node.position.x)
				sheet.write(current_row,5, node.position.y)
				current_row += 1

	book.save("panels_by_nodes.xls")
