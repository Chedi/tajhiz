from django.contrib            import admin
from django.conf.urls.defaults import *
from tajhiz.saisie.views       import *
import tajhiz.saisie.urls

admin.autodiscover()

urlpatterns = patterns('',
	(r'^admin/'          , include(admin.site.urls)),
	(r'^admin_tools/'    , include('admin_tools.urls')),
	(r'^photologue/'     , include('photologue.urls')),
	(r'^contact/'        , include('contact_form.urls')),
	(r'^tajhiz/'         , include(tajhiz.saisie.urls)),
	(r'^logout/$'        , saisie_logout),
	(r'^accounts/login/$', 'django.contrib.auth.views.login'),
	(r'^$'               , include(tajhiz.saisie.urls)),
)