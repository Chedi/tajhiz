from django.forms             import *
from tajhiz.saisie.utils      import *
from tajhiz.saisie.models     import *
from django.contrib           import admin
from django.forms.widgets     import PasswordInput
from django.contrib.gis.admin import OSMGeoAdmin

class TrackingAdmin(OSMGeoAdmin):
    list_display        = ('gpx_file', 'collecte' ,'parcour',)
    list_editable       = ('parcour' ,)
    list_display_links  = ('collecte',)
    search_fields       = ('collecte',)
    list_per_page       = 10
    ordering            = ('collecte',)
    list_filter         = ('collecte',)
    save_as             = True
    list_select_related = True
    fieldsets = (
      ('Informations' ,       {'fields': ['gpx_file','collecte'], 'classes': ('show','extrapretty')}),
      ('Contour Geomertique', {'fields': ['parcour'            ], 'classes': ('show', 'wide'      )}),
    )
    scrollable = False
    map_width  = 400
    map_height = 325

class DirectionRegionaleAdmin(OSMGeoAdmin):
    list_display        = ('label', 'description', 'geom',)
    list_editable       = ('geom' ,)
    list_display_links  = ('label',)
    search_fields       = ('label',)
    list_per_page       = 10
    ordering            = ('label',)
    list_filter         = ('label',)
    save_as             = True
    list_select_related = True
    fieldsets = (
      ('Informations' ,       {'fields': ['label','utilisateurs','description'], 'classes': ('show','extrapretty')}),
      ('Contour Geomertique', {'fields': ['geom'                              ], 'classes': ('show', 'wide'      )}),
    )
    scrollable = False
    map_width  = 400
    map_height = 325

class SignalRoutierAdmin(OSMGeoAdmin):
    ordering            = ('position'                          ,)
    list_filter         = ('type', 'etat'                      ,)
    search_field        = ('type', 'etat'                      ,)
    list_display        = ('type', 'etat', 'medias', 'position',)
    list_editable       = ('type', 'etat', 'position'          ,)
    list_display_links  = ('medias'                            ,)
    list_per_page       = 16
    save_as             = True
    list_select_related = True
    fieldsets = (
      ('Informations' ,       {'fields': ['type','etat','medias'], 'classes': ('show','extrapretty')}),
      ('Contour Geomertique', {'fields': ['position'            ], 'classes': ('show', 'wide'      )}),
    )
    scrollable = False
    map_width  = 200
    map_height = 200

class NoeudAdmin(OSMGeoAdmin):
    list_display       = ( 'label', 'type', 'position',)
    list_editable      = ( 'type', 'position',)
    list_display_links = ( 'label',)
    save_as            = True
    ordering           = ( 'label', 'type',)
    fieldsets = (
      ('Informations', {'fields': ['label','type'], 'classes': ('show','extrapretty')}),
      ('Position'    , {'fields': ['position'    ], 'classes': ('show', 'wide'      )}),
    )
    map_width  = 200
    map_height = 200

class VoieAdmin(OSMGeoAdmin):
    list_display       = ( 'label', 'nom_usage', 'largeur', 'cycle', 'geom',)
    list_editable      = ( 'largeur', 'cycle', 'geom',)
    list_display_links = ( 'label',)
    save_as            = True
    ordering           = ( 'label', 'nom_usage',)
    fieldsets = (
      ('Nom',          {'fields': ['label','nom_usage'], 'classes': ('show','extrapretty')}),
      ('Informations', {'fields': ['largeur','cycle'  ], 'classes': ('show','extrapretty')}),
      ('Position'    , {'fields': ['geom'             ], 'classes': ('show', 'wide'      )}),
    )
    map_width  = 400
    map_height = 400

class MediaForm(ModelForm):
    class Meta:
        Model   = Media
        widgets = {
            'image' : AdminImageWidget,
            'audio' : AudioFileWidget ,
        }

class MediaAdmin(admin.ModelAdmin):
    form         = MediaForm
    list_display = ('image_container', 'audio_container', 'image_valide', 'audio_valide')
    fieldsets    = [
        ('Medias', {'fields' : ['image', 'audio'              ]}),
        ('Etat'  , {'fields' : ['image_valide', 'audio_valide']}),
    ]
    list_per_page = 16
