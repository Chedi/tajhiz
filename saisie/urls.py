from tajhiz.saisie.views       import *
from django.conf.urls.defaults import *

urlpatterns = patterns('',
	(r'^$'                                           , welcome                  ),
	(r'^galleries/$'                                 , gallery_browser          ),
	(r'^qualification/(?P<gallery_title>.+)/$'       , qualification_photo_form ),
	(r'^get_gallery_lock/(?P<gallery_title>.+)/$'    , get_gallery_lock         ),
	(r'^release_gallery_lock/(?P<gallery_title>.+)/' , release_gallery_lock     ),
	(r'^qualification/$'                             , gallery_browser          ),
	(r'^save_qualifications/$'                       , save_qualifications      ),
)