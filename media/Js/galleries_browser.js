/******************************************************************************
 * Some const
 ******************************************************************************/
var QUALIFICATION_URL    = '/tajhiz/qualification/'   ;
var GET_GALLERY_LOCK_URL = '/tajhiz/get_gallery_lock/';

/******************************************************************************
 * Initialization function
 ******************************************************************************/
$(document).ready( function() {
	$(".gallery a").live('click', function(){
		var gallery_name = $(this)[0].name;
		$.ajax({
			url     : GET_GALLERY_LOCK_URL+gallery_name+'/',
			type    : POST_ACTION,
			error   : DEFAULT_AJAX_ERROR_HANDLER,
            success : function(data){
                operation_report_config = {}
                operation_report_config[OR_DATA_KEY      ] = data;
                operation_report_config[OR_SUCCESS_FN_KEY] = function(){ redirect_to(QUALIFICATION_URL+gallery_name+'/'); }
                operation_report(operation_report_config);
        }});
        return false;
})});